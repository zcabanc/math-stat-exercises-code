### A Pluto.jl notebook ###
# v0.12.18

using Markdown
using InteractiveUtils

# ╔═╡ e2a7d83c-5848-11eb-12c4-d3ad998fe15b
begin
	using Statistics
	using Plots
	using StatsKit
	html"<hr><div style=\"height: 120px;\"></div><p>Util functions and imports</p>"
end

# ╔═╡ c3a9b85e-5848-11eb-121d-8374ab7ee04c
md"## Question 2"

# ╔═╡ f0e241d0-5848-11eb-380c-2107ee568f1f
q2arr = [0, 3, 1, 3, 0, 0, 1, 2, 5, 1, 1, 0, 2, 3, 3, 2, 0, 1, 1, 0, 1, 0, 1, 3, 1]

# ╔═╡ 0b3fb012-5849-11eb-2112-8b8569248b4b
Plots.bar(countmap(q2arr),label="")

# ╔═╡ de0c5b1c-5849-11eb-0f72-cb816057cde2
md"""
Mean = $(mean(q2arr)) \
Median = $(median(q2arr)) \
Mode = $(mode(q2arr))
"""

# ╔═╡ 5918bde0-584b-11eb-1568-c92e43c15fbd
q2sorted = sort(q2arr)

# ╔═╡ 00c8537c-584f-11eb-1b16-9daa6d328310
var(q2arr) |> x->round(x; digits=2)

# ╔═╡ 1e8abc6a-584f-11eb-3692-af9baa1edea1
md"
----
## Question 3
"

# ╔═╡ 29f78a9c-584f-11eb-2308-619245fbfad5
q3arr = [120,125,130,150,150,155,160,175,180,190,200,200,200,210,220,225,240,350,500,4950]

# ╔═╡ 45a5b1e2-584f-11eb-3d2d-c3ebf125ca88
Plots.histogram(q3arr,xticks=range(0,maximum(q3arr)+100;step=100),xlim=(0,maximum(q3arr)+100),size=(2000,400))

# ╔═╡ 6a71f8be-584f-11eb-300b-19fd462a2630
md"
Mean = $(mean(q3arr)) \
Median = $(median(q3arr)) \
Median is more representative
"

# ╔═╡ 21f3983a-5850-11eb-00e1-dd15941e5317
md"
------
## Question 4
<del>Probability</del>Propotion of \"1\"s.
"

# ╔═╡ 724cc96e-5850-11eb-1e3f-7dc7cdb86218
md"
------
## Question 7
"

# ╔═╡ 78f7070c-5850-11eb-0ec8-f5489ffd7ea6
q7arr = [66, 70, 69, 80, 68, 67, 72, 73, 70, 57, 63, 70, 78, 67, 53, 67, 75, 70, 81, 76, 79, 75, 76, 58, 31]

# ╔═╡ 80cc848e-5850-11eb-00ab-6555b992d1d8
md"
Mean = $(mean(q7arr)) \
Var = $(var(q7arr) |> x -> round(x; digits=2))
"

# ╔═╡ 9c761326-5850-11eb-1b62-4dfc7888ffa9
q7cels = [18.9, 21.1, 20.6, 26.7, 20.0, 19.4, 22.2, 22.8, 21.1, 13.9, 17.2, 21.1, 25.6, 19.4, 11.7, 19.4,23.9, 21.1, 27.2, 24.4, 26.1, 23.9, 24.4, 14.4, -0.6]

# ╔═╡ a2f98638-5850-11eb-2112-fd99f2c54d0d
md"
Mean = $(mean(q7cels)) \
Var = $(var(q7cels) |> x -> round(x; digits=2))
"

# ╔═╡ b14c6e78-5850-11eb-0e87-279d2f30d61c
(5/9)^2

# ╔═╡ b845810c-5850-11eb-359e-5da004bf8735
var(q7cels)/var(q7arr)

# ╔═╡ f5889ccc-5850-11eb-03a1-85ec0000b134
md"
Mean relationship: $\frac{5}{9}\left(\overline{t_\text{F}}-32\right)$ \
Variance relationship: $\left(\frac{5}{9}\right)^2 s_\text{F}^2$
"

# ╔═╡ a39ba3d2-584b-11eb-3f42-37d962ae4877
function quartile_indices(len::Int64)
	medindex = (1+len)/2
	if (1+len)%2 == 1
		# 1 2 3 4
		left = floor(medindex)
		right = ceil(medindex)
	else
		# 1 2 3 4 5
		left = medindex - 1
		right = medindex + 1
	end
	((1+left)/2, medindex, (right+len)/2)
end

# ╔═╡ f225f376-584c-11eb-3f2e-23393a343d88
function arrget(arr, i)
	i_int = round(i)
	i_left = floor(Int64, i)
	i_right = ceil(Int64, i)
	if abs(i_int - i) < 0.0001
		arr[round(Int64, i)]
	else
		(arr[i_left] + arr[i_right])/2
	end
end

# ╔═╡ ec204052-584d-11eb-3671-3d2e593eb343
function quartiles(arr)
	if !issorted(arr)
		throw("Not sorted")
	end
	(x->arrget(arr,x)).(quartile_indices(length(arr)))
end

# ╔═╡ 920e4b28-584c-11eb-10af-9b8377cec0f8
begin
	local qs
	qs = quartiles(q2sorted)
	md"1st Q: $(qs[1]), 3rd Q: $(qs[3]), IQR: $(qs[3] - qs[1])"
end

# ╔═╡ Cell order:
# ╟─c3a9b85e-5848-11eb-121d-8374ab7ee04c
# ╟─f0e241d0-5848-11eb-380c-2107ee568f1f
# ╠═0b3fb012-5849-11eb-2112-8b8569248b4b
# ╠═de0c5b1c-5849-11eb-0f72-cb816057cde2
# ╟─5918bde0-584b-11eb-1568-c92e43c15fbd
# ╠═920e4b28-584c-11eb-10af-9b8377cec0f8
# ╠═00c8537c-584f-11eb-1b16-9daa6d328310
# ╟─1e8abc6a-584f-11eb-3692-af9baa1edea1
# ╠═29f78a9c-584f-11eb-2308-619245fbfad5
# ╟─45a5b1e2-584f-11eb-3d2d-c3ebf125ca88
# ╠═6a71f8be-584f-11eb-300b-19fd462a2630
# ╠═21f3983a-5850-11eb-00e1-dd15941e5317
# ╠═724cc96e-5850-11eb-1e3f-7dc7cdb86218
# ╟─78f7070c-5850-11eb-0ec8-f5489ffd7ea6
# ╠═80cc848e-5850-11eb-00ab-6555b992d1d8
# ╟─9c761326-5850-11eb-1b62-4dfc7888ffa9
# ╟─a2f98638-5850-11eb-2112-fd99f2c54d0d
# ╠═b14c6e78-5850-11eb-0e87-279d2f30d61c
# ╠═b845810c-5850-11eb-359e-5da004bf8735
# ╠═f5889ccc-5850-11eb-03a1-85ec0000b134
# ╟─e2a7d83c-5848-11eb-12c4-d3ad998fe15b
# ╟─a39ba3d2-584b-11eb-3f42-37d962ae4877
# ╟─f225f376-584c-11eb-3f2e-23393a343d88
# ╟─ec204052-584d-11eb-3671-3d2e593eb343
